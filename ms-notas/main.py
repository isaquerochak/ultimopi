from google.cloud import datastore
import time
import json
import base64
from google.cloud import pubsub_v1
from flask import Flask
from flask import request
from flask import jsonify

import logging

app = Flask(__name__)
@app.route('/')
def hello():
    cpf = request.args.get('cpf')

    return getNota(cpf)

@app.route('/pubsub/push', methods=['POST'])
def pubsub_push():
	try:
		envelope = json.loads(request.data.decode('utf-8'))
		print(envelope)
		payload = base64.b64decode(envelope['message']['data']).decode('utf-8')
		print(payload)
		nota = json.loads(payload)
		print(' ')
		print('Mensagem recebida: {}'.format(nota))
		print(' ')
		saveNota(nota['cpf'],nota['nota'])
	except:
		print('erro')
	
	return 'OK', 200    

def saveNota(cpf,nota):
    datastore_client = datastore.Client()

    kind = 'nota'
    idKey= cpf
    task_key = datastore_client.key(kind, idKey)

    # Prepares the new entity
    task = datastore.Entity(key=task_key)
    task['nota'] = nota

    # Saves the entity
    datastore_client.put(task)

    print('Registro salvo no DataStore {}: {}'.format(task.key.name, task['nota']))

def getNota(key):

    datastore_client = datastore.Client()

    kind = 'nota'

    task_key = datastore_client.key(kind, key)

    result = datastore_client.get(task_key)

    response = {
        "cpf":result.key.name,
        "nota":float(result['nota'].replace(',','.'))
    }

    return jsonify(response)
'''
def receive_messages():
    print('Startando subscriber')

    project_id = "ultimopi2018-223901"
    subscription_name = "snota"

    subscriber = pubsub_v1.SubscriberClient()
    #topic_path = subscriber.topic_path(project_id, 'nota')

    subscription_path = subscriber.subscription_path(
        project_id, subscription_name)

    def callback(message):
        try:
            print(' ')
            print('Mensagem recebida: {}'.format(message))
            print(' ')
            msgJson = message.data.decode('utf-8')
            #print(msgJson)
            nota = json.loads(msgJson)
            #print(nota['cpf'])
            saveNota(nota['cpf'],nota['nota'])
            message.ack()
        except:
            print('Mensagem com erro')
            message.ack()

    subscriber.subscribe(subscription_path, callback=callback)

    print('Ouvindo mensagens em {}'.format(subscription_path))
    while True:
        time.sleep(60)'''
        
@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500


if __name__ == "__main__": 
    ''' t1 = threading.Thread(target=api) 
    t2 = threading.Thread(target=receive_messages) 

    t1.start() 
    t2.start() 

    t1.join() 
    t2.join() 

    print("Finalizado!") '''
    app.run(host='127.0.0.1', port=8080, debug=False)