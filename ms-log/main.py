import time
from google.cloud import bigquery
import time
import json
from google.cloud import pubsub_v1

def saveNota(nota):
    bigquery_client = bigquery.Client()

    dataset_ref = bigquery_client.dataset('logs')
    table_ref = dataset_ref.table('log_notas')
    table = bigquery_client.get_table(table_ref)

    rows_to_insert = [
        (nota['user'],nota['cpf'],nota['nota'].replace(',','.'),nota['op'],nota['datahora'])
    ]
    
    errors = bigquery_client.insert_rows(table, rows_to_insert)  # API request
    print(errors)


def receive_messages():
    print('Startando subscriber')

    project_id = "ultimopi2018-223901"
    subscription_name = "lnota"

    subscriber = pubsub_v1.SubscriberClient()
    #topic_path = subscriber.topic_path(project_id, 'nota')

    subscription_path = subscriber.subscription_path(
        project_id, subscription_name)

    def callback(message):
        try:
            print(' ')
            print('Mensagem recebida: {}'.format(message))
            print(' ')
            msgJson = message.data.decode('utf-8')
            #print(msgJson)
            nota = json.loads(msgJson)
            #print(nota['cpf'])
            saveNota(nota)
            message.ack()
        except:
            print('Mensagem com erro')
            message.ack()

    subscriber.subscribe(subscription_path, callback=callback)

    print('Ouvindo mensagens em {}'.format(subscription_path))
    while True:
        time.sleep(60)

if __name__ == "__main__": 
    receive_messages()