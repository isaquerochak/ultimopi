from flask import Flask
from flask import request
from google.cloud import pubsub_v1
import datetime

app = Flask(__name__)

@app.route('/')
def hello():
    """Return a friendly HTTP greeting."""
    cpf = request.args.get('cpf')
    nota = request.args.get('nota')
    user = request.args.get('user')
    publishUser(cpf,nota,user)

    return 'Lançando nota aluno= ' + cpf + ' nota=' + nota


def publishUser(cpf, nota, user):
    import json

    # publisher client
    publisher = pubsub_v1.PublisherClient()
    topic_name = 'notas'

    project_id = "ultimopi2018-223901"

    topic_path = publisher.topic_path(project_id, topic_name)
    
    print(datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

    # Montagem do json
    data = {
        	"cpf": cpf,
        	"nota": nota,
            "user": user,
            "op": "1",
            "datahora":datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        }
    # transformando json em bytestring com aspas duplas
    data = str(json.dumps(data)).encode('utf-8')

    # Publicando mensagem no topico
    future = publisher.publish(topic_path, data=data)
    print(' ')
    print('Publicado {} mensagem ID {}.'.format(data, future.result()))
    print(' ')
    
if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App
    # Engine, a webserver process such as Gunicorn will serve the app. This
    # can be configured by adding an `entrypoint` to app.yaml.
    app.run(host='127.0.0.1', port=8081, debug=True)